const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.conf')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

function cssLoader (extraLoader) {
  let cssLoaderBase = [
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1
      }
    },
    'postcss-loader'
  ]
  return extraLoader ? cssLoaderBase.concat([{
    loader: extraLoader
  }]) : cssLoaderBase
}

module.exports = merge.smart(base, {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: ExtractTextPlugin.extract({
              loader: 'vue-style-loader!css-loader!sass-loader'
            }),
            sass: ExtractTextPlugin.extract({
              loader: 'vue-style-loader!css-loader!sass-loader'
            })
          }
        }
      },
      {
        test: /\.s[a|c]ss$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader('sass-loader')
        })
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader('stylus-loader')
        })
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          loader: cssLoader()
        })
      }
    ]
  },
  resolve: {
    // tree-shaking
    mainFields: ['jsnext:main', 'main']
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css'
    })
  ]
})
