const webpack = require('webpack')
const helpers = require('./helpers')
const root = helpers.root

function cssLoader (extraLoader) {
  let cssLoaderBase = [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1
      }
    },
    'postcss-loader'
  ]
  return extraLoader ? cssLoaderBase.concat([{
    loader: extraLoader
  }]) : cssLoaderBase
}

module.exports = {
  context: __dirname,
  entry: {
    'vulma': [
      root('src/vulma.js'),
      root('src/vulma.sass')
    ]
  },
  output: {
    path: root('dist'),
    publicPath: '/dist/',
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this nessessary.
            scss: 'vue-style-loader!css-loader!sass-loader',
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.s[a|c]ss$/,
        use: cssLoader('sass-loader')
      },
      {
        test: /\.styl$/,
        use: cssLoader('stylus-loader')
      },
      {
        test: /\.css$/,
        use: cssLoader()
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: 'images/[name].[hash:6].[ext]'
        }
      },
      {
        test: /\.(woff|woff2|svg|eot|ttf)\??.*$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: 'fonts/[name].[hash:6].[ext]'
        }
      }
    ]
    // Mark: use 'noParse' to skip parse modules
  },
  resolve: {
    // options for resolving module requests
    // (does not apply to resolving to loaders)

    // extensions that are used
    extensions: ['.js', '.json', '.vue', '.md'],

    alias: {
      // a list of module name aliases

      'src': root('src'),
      'docs': root('docs'),
      'components': root('src/components')
    },
    // directories where to look for modules
    modules: [
      root('node_modules'),
      root('client')
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
      }
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ]
}
