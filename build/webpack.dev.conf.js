const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.conf')
const helpers = require('./helpers')
const md = require('markdown-it')()
const striptags = require('./strip-tags')
const root = helpers.root

function convert (str) {
  str = str.replace(/(&#x)(\w{4});/gi, function ($0) {
    return String.fromCharCode(parseInt(encodeURIComponent($0).replace(/(%26%23x)(\w{4})(%3B)/g, '$2'), 16))
  })
  return str
}

base.entry.docs = root('docs/app.js')

module.exports = merge.smart(base, {
  module: {
    rules: [
      {
        test: /\.md$/,
        loader: 'vue-markdown-loader',
        options: {
          preset: 'default',
          html: true,
          breaks: true,
          use: [
            [require('markdown-it-container'), 'demo', {
              validate: function (params) {
                return params.trim().match(/^demo\s*(.*)$/)
              },

              render: function (tokens, idx) {
                if (tokens[idx].nesting === 1) {
                  let summaryContent = tokens[idx + 1].content
                  let summary = striptags.fetch(summaryContent, 'summary')
                  let summaryHTML = summary ? md.render(summary) : ''

                  let content = tokens[idx + 2].content
                  let html = convert(striptags.strip(content, ['script', 'style'])).replace(/(<[^>]*)=""(?=.*>)/g, '$1')
                  let script = striptags.fetch(content, 'script')
                  let style = striptags.fetch(content, 'style')
                  let code = tokens[idx + 2].markup + tokens[idx + 2].info + '\n' + content + tokens[idx + 2].markup
                  let codeHtml = code ? md.render(code) : ''

                  let jsfiddle = { html: html, script: script, style: style }
                  jsfiddle = md.utils.escapeHtml(JSON.stringify(jsfiddle))

                  // opening tag
                  return `<demo-box :jsfiddle="${jsfiddle}">
                    <div class="box-demo-show" slot="component">${html}</div>
                    <div slot="description">${summaryHTML}</div>
                    <div class="highlight" slot="code">${codeHtml}</div>
                  `
                } else {
                  // closing tag
                  return '</demo-box>\n'
                }
              }
            }]
          ]
        }
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  devtool: 'cheap-module-source-map',
  performance: {
    hints: false
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ]
})

