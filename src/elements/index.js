import A from './A.vue'
import Button from './Button.vue'
import ButtonGroup from './ButtonGroup.vue'
import Icon from './Icon.vue'
import IconFa from './IconFa.vue'
import Switch from './Switch.vue'
import Tag from './Tag.vue'

const BulmaElement = {
  A,
  Button,
  ButtonGroup,
  Icon,
  IconFa,
  Switch,
  Tag
}

export default BulmaElement
