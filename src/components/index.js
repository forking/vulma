import { Level, LevelItem } from './Level'
import Media from './Media'
import Modal from './Modal'
import Notify from './Notify'
import Affix from './Affix'

const BulmaComponents = {
  Level,
  LevelItem,
  Media,
  Modal,
  Notify,
  Affix
}

export default BulmaComponents
