import BulmaElements from './elements'
import BulmaComponents from './components'

const { Notify } = BulmaComponents

delete BulmaComponents['Notify']

export default {
  version: '0.0.1',
  install (Vue, options = {}) {
    let opts = {
      prefix: 'B'
    }
    Object.assign(opts, options)
    // capitalize first letter
    opts.prefix = opts.prefix.charAt(0).toUpperCase() + opts.prefix.slice(1)

    Object.keys(BulmaElements).forEach(key => Vue.component(`${opts.prefix}${key}`, BulmaElements[key]))

    Object.keys(BulmaComponents).forEach(key => Vue.component(`${opts.prefix}${key}`, BulmaComponents[key]))

    Vue.prototype.$notify = Notify
    // Vue.prototype.$modal = MessageModal
  }
}
