# vulma

> Bulma UI library for Vuejs 2.0
> vulma = vue + bulma

## Install
``` bash
yarn add bitbucket:forking/vulma
```

``` javascript
import 'vulma/src/vulma.sass'
import Vulma from 'vulma/src/vulma'

Vue.use(Vulma)
```

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
yarn run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
