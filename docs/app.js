import 'src/vulma.sass'
import './app.sass'
import Vue from 'vue'
import Vulma from 'src/vulma'
import DemoBox from 'docs/components/DemoBox'
import Router from 'vue-router'
import routes from './routes'
import App from './App'

Vue.use(Vulma)
Vue.use(Router)

Vue.component('demo-box', DemoBox)

const router = new Router({
  mode: 'history',
  routes
})

// create the app instance.
new Vue({
  router,
  render: h => h(App)
}).$mount('app')
