import RoutesEn from './routes.en'
import RoutesZh from './routes.zh'

let routes = [{
  path: '/',
  name: 'home',
  component: require('docs/pages/Index')
}]
  .concat(
    RoutesEn,
    RoutesZh,
  {
    path: '*',
    name: 'pageNotFound',
    component: require('docs/pages/PageNotFound')
  }
  )

export default routes
