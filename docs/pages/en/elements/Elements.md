## Tags
Small **tag labels** to insert anywhere

---

::: demo
<summary>
  * By default, a **tag** is a 1.5rem high label.
</summary>

``` html
<b-tag>Tag label</b-tag>
<b-tag type="black">Black</b-tag>
<b-tag type="dark">Dark</b-tag>
<b-tag type="light">Light</b-tag>
<b-tag type="white">White</b-tag>
<b-tag type="primary">Primary</b-tag>
<b-tag type="info">Info</b-tag>
<b-tag type="success">Success</b-tag>
<b-tag type="warning">Warning</b-tag>
<b-tag type="danger">Danger</b-tag>
<br><br>
<b-tag type="primary" size="medium">Medium</b-tag>
<b-tag type="info" size="large">Large</b-tag>
<b-tag type="success" closable>Bar</b-tag>
<b-tag type="warning" size="medium" closable>Hello</b-tag>
<b-tag type="danger" size="large" closable>World</b-tag>
```
:::

## Switch
<b-switch type="primary" size="small" checked></b-switch>
<b-switch onText="On" offText="Off" type="info" checked></b-switch>
<b-switch onText="On" offText="Off" type="success" checked></b-switch>
<b-switch type="warning" size="small" checked disabled></b-switch>
<b-switch onText="On" offText="Off" type="danger" checked disabled></b-switch>

## Icons

Bulma is compatible with **[Font Awesome](http://fortawesome.github.io/Font-Awesome/)** icons.
---
Font Awesome icons use a font-size of **28px** by default, and are best rendered when using **multiples of 7**.
The Bulma `icon` container is always slightly bigger than the font-size used:

::: demo
<summary>
  #### icons
  * size: `small` default `medium` `large`
  * <b-icon type="fa fa-home"></b-icon>
  * <b-icon-fa type="home"></b-icon-fa>
</summary>

```html
<b-icon type="fa fa-home" size="small"></b-icon>
small - 14px - 1rem x 1rem
<br><br>
<b-icon type="fa fa-home"></b-icon>
default - 21px - 1.5rem x 1.5rem
<br><br>
<b-icon-fa type="home" size="medium"></b-icon-fa>
medium - 28px - 2rem x 2rem
<br><br>
<b-icon-fa type="home" size="large"></b-icon-fa>
large - 42px - 3rem x 3rem
```
:::
