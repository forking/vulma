## Notifications
Bold **notification** blocks, to alert your users of something

---

::: demo
<summary>
  #### 基本
  * 默认有六种类型，除了default还包括`primary` `info` `success` `danger` `warning`，通过`type`来设置，也可通过`this.$notify.info({...})`的形式直接调用
  * 默认 4.5秒 自动关闭
</summary>

``` html
<b-a @click.native="basicNotify">Click me</b-a>
<b-a type="primary" @click.native="primaryNotify">Click me</b-a>
<b-a type="info" @click.native="infoNotify">Click me</b-a>
<b-a type="success" @click.native="successNotify">Click me</b-a>
<b-a type="warning" @click.native="warningNotify">Click me</b-a>
<b-a type="danger" @click.native="dangerNotify">Click me</b-a>

<script>
export default {
  methods: {
    basicNotify () {
      this.$notify.open({content: '您点击了我！' })
    },
    primaryNotify () {
      this.$notify.open({content: '您点击了我！', type: 'primary'})
    },
    infoNotify () {
      this.$notify.info({content: '您点击了我！'})
    },
    successNotify () {
      this.$notify.success({content: '您点击了我！'})
    },
    warningNotify () {
      this.$notify.warning({content: '您点击了我！'})
    },
    dangerNotify () {
      this.$notify.open({content: '您点击了我！', type: 'danger'})
    }
  }
}
</script>
```
:::

<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column">
        <div class="notification">
          <button class="delete"></button>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
        <div class="notification is-primary">
          <button class="delete"></button>
          Info lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
        <div class="notification is-info">
          <button class="delete"></button>
          Info lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
        <div class="notification is-success">
          <button class="delete"></button>
          Success lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
        <div class="notification is-warning">
          <button class="delete"></button>
          Warning lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
        <div class="notification is-danger">
          <button class="delete"></button>
          Danger lorem ipsum dolor sit amet, consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit
        </div>
      </div>
    </div>
  </div>
</section>

<script>
export default {
  methods: {
    basicNotify () {
      this.$notify.open({message: 'basic!' })
    },
    primaryNotify () {
      this.$notify.open({message: 'primary!', type: 'primary'})
    },
    infoNotify () {
      this.$notify.info({message: 'info!'})
    },
    successNotify () {
      this.$notify.success({message: 'success!'})
    },
    warningNotify () {
      this.$notify.warning({message: 'warning!'})
    },
    dangerNotify () {
      this.$notify.open({message: 'danger!', type: 'danger'})
    }
  }
}
</script>
