## Buttons

The classic **button**, in different colors, sizes, and states

---

::: demo
<summary>
  #### Buttons
  * Types: `default` `white` `light` `dark` `black` `link` `primary` `info` `success` `warning` `danger`
  * Size: `small` `medium` `large`
  * Styles: `outlined` `inverted`
  * States: `hovered` `focused` `active` `loading` `disabled`
</summary>

```html
<h4 class="title">Types</h4>
<b-a>Default</b-a>
<b-a type="white">White</b-a>
<b-a type="light">Light</b-a>
<b-a type="dark">Dark</b-a>
<b-a type="black">Black</b-a>
<b-a type="link">Link</b-a>
<b-a type="primary">Primary</b-a>
<b-a type="info">Info</b-a>
<b-a type="success">Success</b-a>
<b-a type="warning">Warning</b-a>
<b-a type="danger">Danger</b-a>
<br><br>
<h4 class="title">Sizes</h4>
<b-a size="small">Small</b-a>
<b-a>Normal</b-a>
<b-a size="medium">Medium</b-a>
<b-a size="large">Large</b-a>
<br><br>
<h4 class="title">Styles</h4>
<b-a outlined>Outlined</b-a>
<b-a type="primary" outlined>Outlined</b-a>
<b-a type="info" outlined>Outlined</b-a>
<b-a type="success" outlined>Outlined</b-a>
<b-a type="danger" outlined>Outlined</b-a>
<br><br>
<div class="block bg-is-primary inverted-block">
  <b-a inverted>Inverted</b-a>
  <b-a type="primary" inverted>Inverted</b-a>
  <b-a type="info" inverted>Inverted</b-a>
  <b-a type="success" inverted outlined>Inverted</b-a>
  <b-a type="danger" inverted outlined>Inverted</b-a>
</div>
<br><br>
<h4 class="title">States</h4>
<b-a>Normal</b-a>
<b-a type="primary" state="hovered">Hover</b-a>
<b-a type="info" state="focused">Focus</b-a>
<b-a type="success" state="active">Active</b-a>
<b-a type="warning" state="loading">Loading</b-a>
<b-a type="danger" state="disabled">Disabled</b-a>
```
:::

<h4 class="subtitle">With Font Awesome icons</h4>
<div class="columns">
  <div class="column">
    <div class="control">
      <b-a>
        <span class="icon is-small">
          <i class="fa fa-bold"></i>
        </span>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-italic"></b-icon>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-underline"></b-icon>
      </b-a>
      <b-a type="success">
        <b-icon size="small" type="fa fa-check"></b-icon>
        <span>Save</span>
      </b-a>
      <b-a type="danger" outlined>
        <span>Delete</span>
        <b-icon size="small" type="fa fa-times"></b-icon>
      </b-a>
    </div>
  </div>
</div>
<div class="columns">
  <div class="column">
    <div class="content">
      <p>
        <span class="tag is-success">New!</span>
      </p>
      <p>
        If the button only contains an icon, Bulma will make sure the button remains <strong>square</strong>, no matter the size of the button <em>or</em> of the icon.
      </p>
    </div>
    <div class="control">
      <b-a size="small">
        <b-icon size="small" type="fa fa-header"></b-icon>
      </b-a>
    </div>
    <div class="control">
      <b-a>
        <b-icon size="small" type="fa fa-header"></b-icon>
      </b-a>
      <b-a>
        <b-icon type="fa fa-header"></b-icon>
      </b-a>
    </div>
    <div class="control">
      <b-a size="medium">
        <b-icon size="small" type="fa fa-header"></b-icon>
      </b-a>
      <b-a size="medium">
        <b-icon type="fa fa-header"></b-icon>
      </b-a>
      <b-a size="medium">
        <b-icon size="medium" type="fa fa-header"></b-icon>
      </b-a>
    </div>
    <div class="control">
      <b-a size="large">
        <b-icon size="small" type="fa fa-header"></b-icon>
      </b-a>
      <b-a size="large">
        <b-icon type="fa fa-header"></b-icon>
      </b-a>
      <b-a size="large">
        <b-icon size="medium" type="fa fa-header"></b-icon>
      </b-a>
      <b-a size="large">
        <b-icon size="large" type="fa fa-header"></b-icon>
      </b-a>
    </div>
  </div>
</div>
<hr>
<h1 class="title">Button group</h1>
<div class="content">
  <p>If you want to <strong>group</strong> buttons together, use the <code>is-grouped</code> modifier on the <code>control</code> container:</p>
</div>
<div class="example">
  <b-button-group>
    <b-a type="primary">Save changes</b-a>
    <b-a>Cancel</b-a>
    <b-a type="danger">Delete post</b-a>
  </b-button-group>
</div>
<hr>
<h3 class="title">Button addons</h3>
<div class="content">
  <p>If you want to use buttons as <strong>addons</strong>, use the <code>has-addons</code> modifier on the <code>control</code> container:</p>
</div>
<div class="example">
  <div class="control has-addons">
    <b-a>
      <b-icon size="small" type="fa fa-align-left"></b-icon>
      <span>Left</span>
    </b-a>
    <b-a>
      <b-icon size="small" type="fa fa-align-center"></b-icon>
      <span>Center</span>
    </b-a>
    <b-a>
      <b-icon size="small" type="fa fa-align-right"></b-icon>
      <span>Right</span>
    </b-a>
  </div>
</div>
<hr>
<h3 class="title">Button group with addons</h3>
<div class="content">
  <p>You can group together addons as well:</p>
</div>
<div class="example">
  <b-button-group>
    <div class="control has-addons">
      <b-a>
        <b-icon size="small" type="fa fa-bold"></b-icon>
        <span>Bold</span>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-italic"></b-icon>
        <span>Italic</span>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-underline"></b-icon>
        <span>Underline</span>
      </b-a>
    </div>
    <div class="control has-addons">
      <b-a>
        <b-icon size="small" type="fa fa-align-left"></b-icon>
        <span>Left</span>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-align-center"></b-icon>
        <span>Center</span>
      </b-a>
      <b-a>
        <b-icon size="small" type="fa fa-align-right"></b-icon>
        <span>Right</span>
      </b-a>
    </div>
  </b-button-group>
</div>

<style lang="stylus">
.inverted-block
  padding: 1.25rem
</style>
