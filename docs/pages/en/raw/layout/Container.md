<template>
<div>
  <section class="section">
    <div class="container">
      <h1 class="title">Container</h1>
      <h2 class="subtitle">
        A simple <strong>container</strong> to center your content horizontally
      </h2>

      <hr>

      <div class="content">
        <p>The <code>.container</code> class can be used in any context, but mostly as a <strong>direct child</strong> of either:</p>
        <ul>
          <li><code>.nav</code></li>
          <li><code>.hero</code></li>
          <li><code>.section</code></li>
          <li><code>.footer</code></li>
        </ul>
        <p>On <strong>mobile</strong> and <strong>tablet</strong>, the container will have a margin of <strong>20px</strong> on both the left and right sides.</p>
        <p>On <strong>desktop</strong> (&gt;= 1000px), the container will have a maximum width of <strong>960px</strong> and will be <strong>horizontally centered</strong>.</p>
        <p>On <strong>widescreen</strong> (&gt;= 1192px), the container will have a maximum width of <strong>1152px</strong>.</p>
        <p>The values <strong>960</strong> and <strong>1152</strong> have been chosen because they are divisible by both <strong>12</strong> and <strong>16</strong>.</p>
      </div>
    </div>

    <div class="example">
      <div class="container">
        <div class="notification">
          This container is <strong>centered</strong> on desktop.
        </div>
      </div>
    </div>

  <figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"container"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"notification"</span><span class="nt">&gt;</span>
      This container is <span class="nt">&lt;strong&gt;</span>centered<span class="nt">&lt;/strong&gt;</span> on desktop.
    <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;/div&gt;</span></code></pre><button class="copy">Copy</button></figure>

    <div class="container">
      <hr>
      <h3 class="title">Fluid container</h3>
      <div class="content">
        <p>If you don't want to have a maximum width but want to keep the 20px margin on the left and right sides, add the <code>is-fluid</code> modifier:</p>
      </div>
    </div>
  </section>

  <div class="example is-fullwidth">
    <div class="container is-fluid">
      <div class="notification">
        This container is <strong>fluid</strong>: it will have a 20px gap on either side.
      </div>
    </div>
  </div>

  <figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"container is-fluid"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"notification"</span><span class="nt">&gt;</span>
    This container is <span class="nt">&lt;strong&gt;</span>fluid<span class="nt">&lt;/strong&gt;</span>: it will have a 20px gap on either side, on any viewport size.
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre><button class="copy">Copy</button></figure>
</div>
</template>
