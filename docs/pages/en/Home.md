<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column is-2 side-nav">
        <nav-view></nav-view>
      </div>
      <div class="column is-10">
        <router-view class="content"></router-view>
      </div>
    </div>
  </div>
</section>


<script>
import NavView from 'docs/pages/en/Nav.md'

export default {
  name: 'en',
  components: {
    NavView
  },
  data () {
    return {}
  }
}
</script>

<style lang="stylus">
.section
  padding: 40px 20px

.side-nav
  border-right: 1px solid #e9e9e9
</style>
