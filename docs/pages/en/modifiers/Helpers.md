<template>
<div>
  <section class="section">
    <div class="container">
      <h1 class="title">Helpers</h1>
      <h2 class="subtitle">You can apply <strong>responsive helper classes</strong> to almost any element, in order to alter its style based upon the browser's width.</h2>
      <hr>
      <table class="table is-bordered">
        <tbody>
          <tr>
            <th rowspan="3">Float</th>
            <td><code>is-clearfix</code></td>
            <td>Fixes an element's floating children</td>
          </tr>
          <tr>
            <td><code>is-pulled-left</code></td>
            <td>Moves an element to the left</td>
          </tr>
          <tr>
            <td><code>is-pulled-right</code></td>
            <td>Moves an element to the right</td>
          </tr>
          <tr>
            <th>Overlay</th>
            <td><code>is-overlay</code></td>
            <td>Completely covers the first positioned parent</td>
          </tr>
          <tr>
            <th>Size</th>
            <td><code>is-fullwidth</code></td>
            <td>Takes up the whole width (100%)</td>
          </tr>
          <tr>
            <th rowspan="3">Text</th>
            <td><code>has-text-centered</code></td>
            <td>Centers the text</td>
          </tr>
          <tr>
            <td><code>has-text-left</code></td>
            <td>Text is left-aligned</td>
          </tr>
          <tr>
            <td><code>has-text-right</code></td>
            <td>Text is right-aligned</td>
          </tr>
          <tr>
            <th rowspan="4">Other</th>
            <td><code>is-disabled</code></td>
            <td>Removes any <strong>click</strong> event</td>
          </tr>
          <tr>
            <td><code>is-marginless</code></td>
            <td>Removes any <strong>margin</strong></td>
          </tr>
          <tr>
            <td><code>is-paddingless</code></td>
            <td>Removes any <strong>padding</strong></td>
          </tr>
          <tr>
            <td><code>is-unselectable</code></td>
            <td>Prevents the text from being <strong>selectable</strong></td>
          </tr>
        </tbody>
      </table>
    </div>
  </section>

  <section class="section">
    <div class="container">
      <h1 class="title">Responsive helpers</h1>
      <h2 class="subtitle"><strong>Show/hide content</strong> depending on the width of the viewport</h2>

      <hr>

      <h3 class="title">Show</h3>

      <div class="content">
        <p>
          You can use one of the following <code>display</code> classes:
        </p>
        <ul>
          <li><code>block</code></li>
          <li><code>flex</code></li>
          <li><code>inline</code></li>
          <li><code>inline-block</code></li>
          <li><code>inline-flex</code></li>
        </ul>
        <p>For example, here's what the <code>is-flex</code> helper works:</p>
      </div>

      <table class="table">
        <thead>
          <tr>
            <th>
              Class
            </th>
            <th>
              Mobile<br>
              Up to <code>768px</code>
            </th>
            <th>
              Tablet<br>
              Between <code>769px</code> and <code>979px</code>
            </th>
            <th>
              Desktop<br>
              Between <code>980px</code> and <code>1179px</code>
            </th>
            <th>
              Widescreen<br>
              Above <code>1180px</code>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="is-narrow">
              <code>is-flex-mobile</code>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-tablet-only</code>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-desktop-only</code>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-widescreen</code>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
          </tr>
          <tr>
            <th colspan="4">
              <p>Classes to display <strong>up to</strong> or <strong>from</strong> a specific breakpoint</p>
            </th>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-touch</code>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-tablet</code>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
          </tr>
          <tr>
            <td class="is-narrow">
              <code>is-flex-desktop</code>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification">unchanged</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
            <td class="is-narrow">
              <p class="notification is-success">flex</p>
            </td>
          </tr>
        </tbody>
      </table>

      <div class="content">
        <p>For the other display options, just replace <code>is-flex</code> with <code>is-block</code> <code>is-inline</code> <code>is-inline-block</code> or <code>is-inline-flex</code>

        </p><hr>

        <h3 class="title">Hide</h3>

        <table class="table">
          <thead>
            <tr>
              <th>
                Class
              </th>
              <th>
                Mobile<br>
                Up to <code>768px</code>
              </th>
              <th>
                Tablet<br>
                Between <code>769px</code> and <code>999px</code>
              </th>
              <th>
                Desktop<br>
                Between <code>1000px</code> and <code>1239px</code>
              </th>
              <th>
                Widescreen<br>
                Above <code>1240px</code>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-mobile</code>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-tablet-only</code>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-desktop-only</code>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-widescreen</code>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
            </tr>
            <tr>
              <th colspan="4">
                <p>Classes to hide <strong>up to</strong> or <strong>from</strong> a specific breakpoint</p>
              </th>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-touch</code>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-tablet</code>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
            </tr>
            <tr>
              <td class="is-narrow">
                <code>is-hidden-desktop</code>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification is-success">visible</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
              <td class="is-narrow">
                <p class="notification">hidden</p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>
</template>
