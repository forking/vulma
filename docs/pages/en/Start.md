Bulma is a modern CSS framework based on Flexbox.

## Start
Use yarn (recommended)
```
yarn add bitbucket:forking/vulma
```
Use in project
```
import 'vulma/src/vulma.sass'
import Vulma from 'vulma/src/vulma'
Vue.use(Vulma)
```
---
## Classes
Bulma is a **CSS** framework, meaning that the end result is simply a **single** `.css` file:
[https://github.com/jgthms/bulma/blob/master/css/bulma.css](https://github.com/jgthms/bulma/blob/master/css/bulma.css)

Because Bulma solely comprises CSS classes, the HTML code you write has **no impact** on the styling of your page. That's why `.input` exists as a class, so you can choose _which_ `<input type="text">` elements you want to style.

Bulma only styles **generic** tags directly **twice**:

*   [`generic.sass`](https://github.com/jgthms/bulma/blob/master/bulma/base/generic.sass) to define a basic style for your page
*   the [`.content` class](http://bulma.io/documentation/elements/content/) to use for _any_ textual content, like WYSIWYG

## Responsiveness

Bulma is a **mobile-first** framework

### Breakpoints

Bulma has 4 breakpoints:

*   `mobile`: up to `768px`
*   `tablet`: from `769px`
*   `desktop`: from `1000px`
*   `widescreen`: from `1192px`

Bulma uses 7 responsive mixins:

*   `=mobile`
    until `768px`
*   `=tablet`
    from `769px`
*   `=tablet-only`
    from `769px` and until `999px`
*   `=touch`
    until `999px`
*   `=desktop`
    from `1000px`
*   `=desktop-only`
    from `1000px` and until `1191px`
*   `=widescreen`
    from `1192px`

How Bulma works is that **everything is mobile-first** by default, and responsive mixins act as _minimum viewport widths_ where some alternative styles are applied.

<table class="table">
  <thead>
    <tr>
      <th>
        Mobile<br>
        Up to <code>768px</code>
      </th>
      <th>
        Tablet<br>
        Between <code>769px</code> and <code>999px</code>
      </th>
      <th>
        Desktop<br>
        Between <code>1000px</code> and <code>1191px</code>
      </th>
      <th>
        Widescreen<br>
        <code>1192px</code> and above
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="is-narrow">
        <p class="notification is-success">mobile</p>
      </td>
      <td class="is-narrow" colspan="3">
        <p class="notification">-</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow">
        <p class="notification">-</p>
      </td>
      <td class="is-narrow" colspan="3">
        <p class="notification is-success">tablet</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow" colspan="2">
        <p class="notification">-</p>
      </td>
      <td class="is-narrow" colspan="3">
        <p class="notification is-success">desktop</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow" colspan="3">
        <p class="notification">-</p>
      </td>
      <td class="is-narrow">
        <p class="notification is-success">widescreen</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow">
        <p class="notification">-</p>
      </td>
      <td class="is-narrow">
        <p class="notification is-success">tablet-only</p>
      </td>
      <td class="is-narrow" colspan="2">
        <p class="notification">-</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow" colspan="2">
        <p class="notification">-</p>
      </td>
      <td class="is-narrow">
        <p class="notification is-success">desktop-only</p>
      </td>
      <td class="is-narrow">
        <p class="notification">-</p>
      </td>
    </tr>
    <tr>
      <td class="is-narrow" colspan="2">
        <p class="notification is-success">touch</p>
      </td>
      <td class="is-narrow" colspan="2">
        <p class="notification">-</p>
      </td>
    </tr>
  </tbody>
</table>

### Vertical by default

Every element in Bulma is **mobile-first** and optmizes for **vertical reading**, so by default on mobile:

*   `columns` are stacked vertically
*   the `level` component will show its children stacked vertically
*   the `nav` menu will be hidden

For example, you can enforce the **horizontal** layout for both `columns` or `nav` by appending the `is-mobile` modifer.

