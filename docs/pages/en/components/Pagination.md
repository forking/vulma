<template>
<section class="section">
  <div class="container">
    <h1 class="title">Pagination</h1>
    <h2 class="subtitle">A responsive, usable, and flexible <strong>pagination</strong></h2>
    <hr>
    <div class="content">
      <p>
        The pagination component consists of several elements:
      </p>
      <ul>
        <li>
          <code>pagination-previous</code> and <code>pagination-next</code> for incremental navigation
        </li>
        <li>
          <code>pagination-list</code> which displays page items:
          <ul>
            <li>
              <code>pagination-link</code> for the page numbers
            </li>
            <li>
              <code>pagination-ellipsis</code> for range separators
            </li>
          </ul>
        </li>
      </ul>
      <p>
        All elements are optional so you can compose your pagination as you wish.
      </p>
    </div>
<div class="example">
<nav class="pagination">
  <a class="pagination-previous">Previous</a>
  <a class="pagination-next">Next page</a>
  <ul class="pagination-list">
    <li>
      <a class="pagination-link">1</a>
    </li>
    <li>
      <span class="pagination-ellipsis">…</span>
    </li>
    <li>
      <a class="pagination-link">45</a>
    </li>
    <li>
      <a class="pagination-link is-current">46</a>
    </li>
    <li>
      <a class="pagination-link">47</a>
    </li>
    <li>
      <span class="pagination-ellipsis">…</span>
    </li>
    <li>
      <a class="pagination-link">86</a>
    </li>
  </ul>
</nav>
</div>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"pagination"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-previous"</span><span class="nt">&gt;</span>Previous<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-next"</span><span class="nt">&gt;</span>Next page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"pagination-list"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>1<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>45<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link is-current"</span><span class="nt">&gt;</span>46<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>47<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>86<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
<span class="nt">&lt;/nav&gt;</span></code></pre><button class="copy">Copy</button></figure>
    <div class="content">
      <p>
        You can disable some links if they are not active, or change the amount of page numbers available.
      </p>
    </div>
<div class="example">
<nav class="pagination">
  <a class="pagination-previous is-disabled">Previous</a>
  <a class="pagination-next">Next page</a>
  <ul class="pagination-list">
    <li>
      <a class="pagination-link is-current">1</a>
    </li>
    <li>
      <a class="pagination-link">2</a>
    </li>
    <li>
      <a class="pagination-link">3</a>
    </li>
  </ul>
</nav>
</div>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"pagination"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-previous is-disabled"</span><span class="nt">&gt;</span>Previous<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-next"</span><span class="nt">&gt;</span>Next page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"pagination-list"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link is-current"</span><span class="nt">&gt;</span>1<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>2<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;</span>
      <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>3<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
<span class="nt">&lt;/nav&gt;</span></code></pre><button class="copy">Copy</button></figure>
    <div class="content">
      <p>
        By default, the list is located on the left, and the previous/next buttons on the right. But you can change the <strong>order</strong> of these elements by using the <code>is-centered</code> and <code>is-right</code> modifiers.
      </p>
    </div>
<div class="example">
<nav class="pagination is-centered">
  <a class="pagination-previous">Previous</a>
  <a class="pagination-next">Next page</a>
  <ul class="pagination-list">
    <li><a class="pagination-link">1</a></li>
    <li><span class="pagination-ellipsis">…</span></li>
    <li><a class="pagination-link">45</a></li>
    <li><a class="pagination-link is-current">46</a></li>
    <li><a class="pagination-link">47</a></li>
    <li><span class="pagination-ellipsis">…</span></li>
    <li><a class="pagination-link">86</a></li>
  </ul>
</nav>
</div>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"pagination is-centered"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-previous"</span><span class="nt">&gt;</span>Previous<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-next"</span><span class="nt">&gt;</span>Next page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"pagination-list"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>1<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>45<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link is-current"</span><span class="nt">&gt;</span>46<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>47<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>86<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
<span class="nt">&lt;/nav&gt;</span></code></pre><button class="copy">Copy</button></figure>
<div class="example">
<nav class="pagination is-right">
  <a class="pagination-previous">Previous</a>
  <a class="pagination-next">Next page</a>
  <ul class="pagination-list">
    <li><a class="pagination-link">1</a></li>
    <li><span class="pagination-ellipsis">…</span></li>
    <li><a class="pagination-link">45</a></li>
    <li><a class="pagination-link is-current">46</a></li>
    <li><a class="pagination-link">47</a></li>
    <li><span class="pagination-ellipsis">…</span></li>
    <li><a class="pagination-link">86</a></li>
  </ul>
</nav>
</div>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"pagination is-right"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-previous"</span><span class="nt">&gt;</span>Previous<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"pagination-next"</span><span class="nt">&gt;</span>Next page<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"pagination-list"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>1<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>45<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link is-current"</span><span class="nt">&gt;</span>46<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>47<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;span</span> <span class="na">class=</span><span class="s">"pagination-ellipsis"</span><span class="nt">&gt;</span><span class="ni">&amp;hellip;</span><span class="nt">&lt;/span&gt;&lt;/li&gt;</span>
    <span class="nt">&lt;li&gt;&lt;a</span> <span class="na">class=</span><span class="s">"pagination-link"</span><span class="nt">&gt;</span>86<span class="nt">&lt;/a&gt;&lt;/li&gt;</span>
  <span class="nt">&lt;/ul&gt;</span>
<span class="nt">&lt;/nav&gt;</span></code></pre><button class="copy">Copy</button></figure>
  </div>
</section>
</template>
<script>
export default {
  data () {
    return {}
  }
}
</script>
