<template>
<section class="section">
  <div class="container">
    <h1 class="title">Card</h1>
    <h2 class="subtitle">An all-around flexible and composable component</h2>
    <hr>
<div class="columns">
  <div class="column is-one-third">
<div class="card">
  <div class="card-image">
    <figure class="image is-4by3">
      <img src="http://bulma.io/images/placeholders/1280x960.png" alt="Image">
    </figure>
  </div>
  <div class="card-content">
    <div class="media">
      <div class="media-left">
        <figure class="image" style="height: 40px; width: 40px;">
          <img src="http://bulma.io/images/placeholders/96x96.png" alt="Image">
        </figure>
      </div>
      <div class="media-content">
        <p class="title is-4">John Smith</p>
        <p class="subtitle is-6">@johnsmith</p>
      </div>
    </div>
    <div class="content">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Phasellus nec iaculis mauris. <a>@bulmaio</a>.
      <a>#css</a> <a>#responsive</a>
      <br>
      <small>11:09 PM - 1 Jan 2016</small>
    </div>
  </div>
</div>
  </div>
  <div class="column">
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card-image"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;figure</span> <span class="na">class=</span><span class="s">"image is-4by3"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"http://bulma.io/images/placeholders/1280x960.png"</span> <span class="na">alt=</span><span class="s">"Image"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;/figure&gt;</span>
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card-content"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"media"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"media-left"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;figure</span> <span class="na">class=</span><span class="s">"image"</span> <span class="na">style=</span><span class="s">"height: 40px; width: 40px;"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"http://bulma.io/images/placeholders/96x96.png"</span> <span class="na">alt=</span><span class="s">"Image"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;/figure&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
      <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"media-content"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"title is-4"</span><span class="nt">&gt;</span>John Smith<span class="nt">&lt;/p&gt;</span>
        <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"subtitle is-6"</span><span class="nt">&gt;</span>@johnsmith<span class="nt">&lt;/p&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"content"</span><span class="nt">&gt;</span>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Phasellus nec iaculis mauris. <span class="nt">&lt;a&gt;</span>@bulmaio<span class="nt">&lt;/a&gt;</span>.
      <span class="nt">&lt;a&gt;</span>#css<span class="nt">&lt;/a&gt;</span> <span class="nt">&lt;a&gt;</span>#responsive<span class="nt">&lt;/a&gt;</span>
      <span class="nt">&lt;br&gt;</span>
      <span class="nt">&lt;small&gt;</span>11:09 PM - 1 Jan 2016<span class="nt">&lt;/small&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre><button class="copy">Copy</button></figure>
  </div>
</div>
    <hr>
<div class="columns">
  <div class="column is-one-third">
<div class="card">
  <header class="card-header">
    <p class="card-header-title">
      Component
    </p>
    <a class="card-header-icon">
      <span class="icon">
        <i class="fa fa-angle-down"></i>
      </span>
    </a>
  </header>
  <div class="card-content">
    <div class="content">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
      <a>@bulmaio</a>. <a>#css</a> <a>#responsive</a>
      <br>
      <small>11:09 PM - 1 Jan 2016</small>
    </div>
  </div>
  <footer class="card-footer">
    <a class="card-footer-item">Save</a>
    <a class="card-footer-item">Edit</a>
    <a class="card-footer-item">Delete</a>
  </footer>
</div>
  </div>
  <div class="column">
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;header</span> <span class="na">class=</span><span class="s">"card-header"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;p</span> <span class="na">class=</span><span class="s">"card-header-title"</span><span class="nt">&gt;</span>
      Component
    <span class="nt">&lt;/p&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"card-header-icon"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"icon"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">"fa fa-angle-down"</span><span class="nt">&gt;&lt;/i&gt;</span>
      <span class="nt">&lt;/span&gt;</span>
    <span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/header&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card-content"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"content"</span><span class="nt">&gt;</span>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
      <span class="nt">&lt;a&gt;</span>@bulmaio<span class="nt">&lt;/a&gt;</span>. <span class="nt">&lt;a&gt;</span>#css<span class="nt">&lt;/a&gt;</span> <span class="nt">&lt;a&gt;</span>#responsive<span class="nt">&lt;/a&gt;</span>
      <span class="nt">&lt;br&gt;</span>
      <span class="nt">&lt;small&gt;</span>11:09 PM - 1 Jan 2016<span class="nt">&lt;/small&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;footer</span> <span class="na">class=</span><span class="s">"card-footer"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"card-footer-item"</span><span class="nt">&gt;</span>Save<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"card-footer-item"</span><span class="nt">&gt;</span>Edit<span class="nt">&lt;/a&gt;</span>
    <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"card-footer-item"</span><span class="nt">&gt;</span>Delete<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;/footer&gt;</span>
<span class="nt">&lt;/div&gt;</span></code></pre><button class="copy">Copy</button></figure>
  </div>
</div>
  </div>
</section>
</template>
<script>
export default {
  data () {
    return {}
  }
}
</script>
