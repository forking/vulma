<template>
<section class="section">
  <div class="container">
    <h1 class="title">Level</h1>
    <h2 class="subtitle">A multi-purpose <strong>horizontal level</strong>, which can contain almost any other element</h2>
    <hr>
    <div class="content">
      <p>The <strong>structure</strong> of a level is the following:</p>
      <ul>
        <li>
          <code>level</code>: main container
          <ul>
            <li><code>level-left</code> for the left side</li>
            <li>
              <code>level-right</code> for the right side
              <ul>
                <li><code>level-item</code> for each individual element</li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
      <p>In a <code>level-item</code>, you can then insert almost <em>anything</em> you want: a title, a button, a text input, or just simple text. No matter what elements you put inside a Bulma <code>level</code>, they will always be <strong>vertically centered</strong>.</p>
    </div>
    <div class="structure">
      <nav class="level structure-item is-structure-container" title="level">
        <div class="level-left structure-item" title="level-left">
          <div class="level-item">
            <p class="subtitle is-5">
              <strong>123</strong> posts
            </p>
          </div>
          <div class="level-item">
            <p class="control has-addons">
              <input class="input" type="text" placeholder="Find a post">
              <button class="button">
                Search
              </button>
            </p>
          </div>
        </div>
        <div class="level-right structure-item" title="level-right">
          <p class="level-item">
            <strong>All</strong>
          </p>
          <p class="level-item">
            <a>Published</a>
          </p>
          <p class="level-item">
            <a>Drafts</a>
          </p>
          <p class="level-item">
            <a>Deleted</a>
          </p>
          <p class="level-item">
            <a class="button is-success">
              New
            </a>
          </p>
        </div>
      </nav>
    </div>
    <div class="example">
      <!-- Main container -->
      <b-level>
        <!-- Left side -->
        <div slot="left">
          <b-level-item>
            <p class="subtitle is-5">
              <strong>123</strong> posts
            </p>
          </b-level-item>
          <b-level-item align="center">
            <p class="control has-addons">
              <input class="input" type="text" placeholder="Find a post">
              <b-button>Search</b-button>
            </p>
          </b-level-item>
        </div>
        <!-- Right side -->
        <div slot="right">
          <b-level-item element="p">
            <strong>All</strong>
          </b-level-item>
          <b-level-item element="p">
            <a>Published</a>
          </b-level-item>
          <b-level-item element="p">
            <a>Drafts</a>
          </b-level-item>
          <b-level-item element="p">
            <a>Deleted</a>
          </b-level-item>
          <b-level-item element="p">
            <a class="button is-success">New</a>
          </b-level-item>
        </div>
      </b-level>
    </div>
    <hr>
    <h3 class="title">Centered level</h3>
    <div class="content">
      If you want a <strong>centered level</strong>, you can use as many <code>level-item</code> as you want, as long as they are <strong>direct</strong> children of the <code>level</code> container.
    </div>
    <div class="example">
      <b-level>
        <b-level-item align="centered">
          <div>
            <p class="heading">Tweets</p>
            <p class="title">3,456</p>
          </div>
        </b-level-item>
        <b-level-item align="centered">
          <div>
            <p class="heading">Following</p>
            <p class="title">123</p>
          </div>
        </b-level-item>
        <b-level-item align="centered">
          <div>
            <p class="heading">Followers</p>
            <p class="title">456K</p>
          </div>
        </b-level-item>
        <b-level-item align="centered">
          <div>
            <p class="heading">Likes</p>
            <p class="title">789</p>
          </div>
        </b-level-item>
      </b-level>
    </div>
    <div class="example">
      <b-level>
        <b-level-item align="centered">
          <a class="link is-info">Home</a>
        </b-level-item>
        <b-level-item align="centered">
          <a class="link is-info">Menu</a>
        </b-level-item>
        <b-level-item align="centered">
          <img src="http://bulma.io/images/bulma-type.png" alt="" style="height: 30px">
        </b-level-item>
        <b-level-item align="centered">
          <a class="link is-info">Reservations</a>
        </b-level-item>
        <b-level-item align="centered">
          <a class="link is-info">Contact</a>
        </b-level-item>
      </b-level>
    </div>
    <hr>
    <h3 class="title">Mobile level</h3>
    <div class="content">
      By default, for space concerns, the level is vertical on mobile. If you want the level to be horizontal on mobile as well, add the <code>is-mobile</code> modifier on the <code>level</code> container.
    </div>
    <div class="example">
      <nav class="level is-mobile">
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Tweets</p>
            <p class="title">3,456</p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Following</p>
            <p class="title">123</p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Followers</p>
            <p class="title">456K</p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Likes</p>
            <p class="title">789</p>
          </div>
        </div>
      </nav>
    </div>
  </div>
</section>
</template>
<script>
export default {
  data () {
    return {}
  }
}
</script>
<style lang="stylus">
positons(top = 0, right = 0, bottom = 0, left = 0)
  top: top
  right: right
  bottom: bottom
  left: left
.example, .structure
  border: 1px solid #ffdd57
  border-radius: 3px
  color: rgba(0, 0, 0, 0.7)
  padding: 1.25rem 1.5rem
  position: relative
  &:before
    background: #ffdd57
    border-radius: 3px 3px 0 0
    bottom: 100%
    content: "Example"
    display: inline-block
    font-size: 7px
    font-weight: bold
    left: -1px
    letter-spacing: 1px
    padding: 3px 5px
    position: absolute
    text-transform: uppercase
    vertical-align: top
  &:not(:first-child)
    margin-top: 2rem
  &:not(:last-child)
    margin-bottom: 1.5rem
.structure
  border-color: #ff3860
  &:before
    background: #ff3860
    color: #fff
    content: "Structure"
.structure-item
  position: relative
  &:before
    position: absolute
    positons()
    background: whitesmoke
    border: 1px solid #dbdbdb
    content: ""
    display: block
    z-index: 1
  &:after
    position: absolute
    positons()
    align-items: center
    content: attr(title)
    display: flex
    justify-content: center
    padding: 3px 5px
    z-index: 2
.structure-item.is-structure-container
  padding: 1.5rem 0.75rem 0.75rem
  &:after
    align-items: flex-start
    justify-content: flex-start
    padding: 0.5rem 0.75rem
</style>
