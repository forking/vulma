<template>
<div>
  <section class="section">
    <div class="container">
      <h1 class="title">Functions</h1>
      <h2 class="subtitle">Utility functions to calculate colors and other values</h2>

      <hr>

      <div class="content">
        <p>Bulma uses 5 custom functions to help define the values and colors dynamically:</p>
        <ul>
          <li><code>powerNumber($number, $exp)</code>: calculates the value of a number exposed to another one. Returns a number.</li>
          <li><code>colorLuminance($color)</code>: defines if a color is dark or light. Return a decimal number between 0 and 1 where &lt;= 0.5 is dark and &gt; 0.5 is light.</li>
          <li><code>findColorInvert($color)</code>: returns either 70% transparent black or 100% opaque white depending on the luminance of the color.</li>
          <li><code>removeUnit($number)</code>: removes the unit of a Sass number. So "10px" becomes "10" and "3.5rem" returns "3.5". Used for string concatenation.</li>
          <li><code>roundToEvenNumber($number)</code>: rounds a number to the closest but lower even one. So 23 becomes 22, and 7.5 returns 6.</li>
        </ul>
      </div>
    </div>
  </section>

  <section class="section">
    <div class="container">
      <h1 class="title">Mixins</h1>
      <h2 class="subtitle">Utility mixins for custom elements and responsive helpers</h2>

      <hr>

      <table class="table is-bordered">
        <tbody><tr>
          <td><code>=arrow($color)</code></td>
          <td>Creates a CSS-only down arrow. Used for the dropdown select.</td>
        </tr>
        <tr>
          <td><code>=block</code></td>
          <td>Defines a margin-bottom of 1.5rem, expect when the element is the last child. Used for almost all block elements.</td>
        </tr>
        <tr>
          <td><code>=clearfix</code></td>
          <td>Adds a clearfix at the end of the element. Used for the "is-clearfix" helper.</td>
        </tr>
        <tr>
          <td><code>=center($size)</code></td>
          <td>Positions an element in the exact center of its parent. Used for the spinner in a loading button.</td>
        </tr>
        <tr>
          <td><code>=delete</code></td>
          <td>Creates a CSS-only cross. Used for the delete element in modals, messages, tags...</td>
        </tr>
        <tr>
          <td><code>=fa($size, $dimensions)</code></td>
          <td>Sets the style of a Font Awesome icon container.</td>
        </tr>
        <tr>
          <td><code>=hamburger($dimensions)</code></td>
          <td>Creates a CSS-only hamburger menu with 3 bars. Used for the "nav-toggle".</td>
        </tr>
        <tr>
          <td><code>=loader</code></td>
          <td>Creates a CSS-only loading spinner. Used for the ".loader" element, and for input and button spinners.</td>
        </tr>
        <tr>
          <td><code>=overflow-touch</code></td>
          <td>Sets the style of a container so that it keeps momentum when scrolling on iOS devices.</td>
        </tr>
        <tr>
          <td><code>=overlay($offset: 0)</code></td>
          <td>Makes the element overlay its parent container, like the transparent modal background.</td>
        </tr>
        <tr>
          <td><code>=placeholder</code></td>
          <td>Sets the styles of an input placeholder.</td>
        </tr>
        <tr>
          <td><code>=unselectable</code></td>
          <td>Turns the element unselectable. Used for buttons to prevent selection when clicking.</td>
        </tr>
        </tbody>
      </table>

      <div class="content">
        <p>These mixins are already used throughout Bulma, but you can use them as well to extend your own styles.</p>
      </div>
    </div>
  </section>
</div>
</template>
