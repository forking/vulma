<aside class="menu">
  <p class="menu-label">Overview</p>
  <ul class="menu-list">
    <li><router-link to="/en">Start</router-link></li>
    <li><router-link to="/en/overview/mixins">Mixins</router-link></li>
    <li><router-link to="/en/overview/variables">Variables</router-link></li>
  </ul>
  <p class="menu-label">Modifiers</p>
  <ul class="menu-list">
    <li><router-link to="/en/modifiers/syntax">Syntax</router-link></li>
    <li><router-link to="/en/modifiers/helpers">Helpers</router-link></li>
  </ul>
  <p class="menu-label">Html&Css</p>
  <ul class="menu-list">
    <li>
      <a>Grid</a>
      <ul>
        <li><router-link to="/en/grid/columns">Columns</router-link></li>
        <li><router-link to="/en/grid/tiles">Tiles</router-link></li>
      </ul>
    </li>
    <li>
      <a>Layout</a>
      <ul>
        <li><router-link to="/en/layout/container">Container</router-link></li>
        <li><router-link to="/en/layout/hero">Hero</router-link></li>
        <li><router-link to="/en/layout/section">Section</router-link></li>
        <li><router-link to="/en/layout/footer">Footer</router-link></li>
      </ul>
    </li>
    <li><router-link to="/en/elements">Elements</router-link></li>
  </ul>
  <p class="menu-label">Elements</p>
  <ul class="menu-list">
    <li><router-link to="/en/elements">Elements</router-link></li>
    <li><router-link to="/en/elements/button">Button</router-link></li>
    <li><router-link to="/en/elements/form">Form</router-link></li>
    <li><router-link to="/en/elements/image">Image</router-link></li>
    <li><router-link to="/en/elements/notification">Notification</router-link></li>
    <li><router-link to="/en/elements/progress">Progress</router-link></li>
    <li><router-link to="/en/elements/table">Table</router-link></li>
  </ul>
  <p class="menu-label">Components</p>
  <ul class="menu-list">
    <li><router-link to="/en/components/card">Card</router-link></li>
    <li><router-link to="/en/components/level">Level</router-link></li>
    <li><router-link to="/en/components/media">Media</router-link></li>
    <li><router-link to="/en/components/menu">Menu</router-link></li>
    <li><router-link to="/en/components/message">Message</router-link></li>
    <li><router-link to="/en/components/modal">Modal</router-link></li>
    <li><router-link to="/en/components/nav">Nav</router-link></li>
    <li><router-link to="/en/components/pagination">Pagination</router-link></li>
    <li><router-link to="/en/components/panel">Panel</router-link></li>
    <li><router-link to="/en/components/tabs">Tabs</router-link></li>
  </ul>
</aside>
